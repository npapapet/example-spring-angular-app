# Example-spring-angular-app
A starter application in spring boot & angular 2.


## Feature:
- LDAP authentication
- SSO authentication
- CSRF protection
- Optimized angular 2 build


## Setting up the spring project
* First we have to install java-8 and make sure that it has the needed policy files
  *  After installing java-8, make sure to replace the JCE policy files with the appropriate ones from:<br >
   http://www.oracle.com/technetwork/java/javase/downloads/jce8-download-2133166.html<br >
   The policy files can be found at $JAVA_HOME/jre/lib/security. The files that you will change are **local_policy.jar** and **US_export_policy.jar**
* Next we need to load CERN certificates in java and our browser: <br >
  *  CERN certificates from https://cafiles.cern.ch/cafiles/certificates/Grid.aspx
    **CERN Root Certification Authority 2.crt** and **CERN Grid Certification Authority.crt**
  * Import certificates to **java cacerts** (Linux instructions) (Default keystore **password** for cacerts is **changeit**):
    <ul>
        <li>Open a console</li>
        <li>sudo keytool -import -file CERN\ Grid\ Certification\ Authority.crt -alias cern_grid -keystore $JAVA_HOME/jre/lib/security/cacerts</li>
        <li>sudo keytool -import -file CERN\ Root\ Certification\ Authority\ 2.crt -alias cern_root -keystore $JAVA_HOME/jre/lib/security/cacerts</li>
    </ul>
  * Import certificates to **Chrome** (Linux instructions):
    <ul>
        <li>Open Chrome settings</li>
        <li>Go to advanced settings and click on the **Manage certificates** button</li>
        <li>Go to the authorities tab and click on import</li>
        <li>Import the **CERN Grid Certification Authority.crt**</li>
    </ul>
* Then we need to set up the hidden file folder and hidden files (default hidden folder is in **/home/nikp/properties**). <br />
  The hidden files are these: <br >
  * A certificate file for accessing the CERN services 
  (default name in the hidden folder: **sample-app.jks**) 
    <ul>
        <li>
        To create it do:
        <ul>
            <li>
                Follow this link https://ca.cern.ch/ca/host/HostSelection.aspx?template=ee2host&instructions=auto and fill in the form using the host url of your server
            </li>
            <li>Open a console</li>
            <li>keytool -importkeystore -srckeystore myCertificate.p12 -destkeystore sample-app.jks -srcstoretype pkcs12</li>
            <li>Copy the sample-app.jks to your hidden folder</li>
        </ul>
        </li>
    </ul>
  * A property file per environment of your application , Dev, Prod, etc... 
  The naming convention is as follows: app-ENVIRONMENT.properties (ex. for dev -> app-dev.properties)<br >
  A sample for this file is in the root folder of the project, named **app-ENVIRONMENT.properties**.
* By now most likely the LDAP authentication should be working. If it's not, then something went wrong in the previous steps <br >
  Now to set up also the SSO authentication, you will have to register the application at https://sso-management.web.cern.ch/SSO/RegisterApplication.aspx
   * It's important that you choose as your **Service Provider type** --> **SAML2 with online metadata**
   * And in the **Application Metadata Uri** you should put https://YOUR-APPLICATION-MAIN-URL/saml/metadata <br >
   (To check that it's the correct URl, by redirecting to the above url you should be getting an xml file)
   * Finally for the **Application Homapage** put this https://YOUR-APPLICATION-MAIN-URL and wait for the SSO team to register your application. 
   It's important that you leave your application running for them to approve it.
* Now you should be ready to start working :) .

## Commands for gradle/Spring
- `gradle clean`: clean the generated build dir
- `gradle bootRun`: download the dependencies & run the application


## Running scripts for Angular
(You have to run these commands from the /front-end/src folder)
- `npm install` to prepare your environment
- `npm run start-aot` to build the angular project and run them at a lite server
- `npm run build-for-spring` to build  the angular project so the Spring app can use it