module.exports = {
    "port": 8000,
    // "files": ["./src/**/*.{html,htm,css,js}"],
    "server": {
        "baseDir": [
            // "./src",
            "./"
        ],
        "middleware": {
            // overrides the second middleware default with new settings
            1: require('connect-history-api-fallback')({
                index: '/index-jit.html',
                verbose: true
            })
        }
    },
};