import nodeResolve from 'rollup-plugin-node-resolve'
import typescript from 'rollup-plugin-typescript';
import commonjs    from 'rollup-plugin-commonjs';
import uglify      from 'rollup-plugin-uglify'

export default {
    entry: 'app/main.js',
    dest: '../../src/main/resources/static/js/build.js', // output a single application bundle
    sourceMap: true,
    sourceMapFile: '../../src/main/resources/static/js/build.js.map',
    format: 'iife',
    plugins: [
        typescript({
            typescript: require('typescript'),
            "experimentalDecorators": true,
            "emitDecoratorMetadata": true
        }),
        nodeResolve({ jsnext: true, module: true }),
        commonjs({
            include: [
                'node_modules/rxjs/**',
                'node_modules/angular-in-memory-web-api/**'
            ],
        }),
        uglify()
    ]
}