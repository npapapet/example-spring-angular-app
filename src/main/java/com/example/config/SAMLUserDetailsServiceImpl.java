package com.example.config;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.saml.SAMLCredential;
import org.springframework.security.saml.userdetails.SAMLUserDetailsService;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

/**
 * Created by nikp on 16.12.16.
 */
@Service
public class SAMLUserDetailsServiceImpl implements SAMLUserDetailsService {

    /**
     * Constants for retrieving information from SAML message.
     */
    protected static final String XMLSOAP_CLAIMS_COMMON_NAME = "http://schemas.xmlsoap.org/claims/CommonName";
    protected static final String XMLSOAP_CLAIMS_GROUP = "http://schemas.xmlsoap.org/claims/Group";

    @Override
    public Object loadUserBySAML(final SAMLCredential credentials) throws UsernameNotFoundException {
        final String niceLogin = credentials.getAttributeAsString(XMLSOAP_CLAIMS_COMMON_NAME).toUpperCase();
        //final String[] eGroupNames = credentials.getAttributeAsStringArray(XMLSOAP_CLAIMS_GROUP);
        return new User(niceLogin, "", true, true ,true ,true, new ArrayList<GrantedAuthority>());
    }
}
