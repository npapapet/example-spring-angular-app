package com.example.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;

/**
 * Created by nikp on 06.12.16.
 */
@Configuration
@Profile("prod")
@PropertySource("file:/home/nikp/properties/app-${spring.profiles.active}.properties")
public class configProd {

    @Autowired
    Environment env;

    /**
     *  A test to see how you can generate beans and also to check that the property file is
     *  loaded properly
     */
    @Bean
    public boolean test() {
        System.out.println(env.getProperty("test.one"));
        return true;
    }
}
