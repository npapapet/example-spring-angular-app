package com.example.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.context.annotation.PropertySource;

/**
 * Created by nikp on 06.12.16.
 */
@Configuration
@Profile("dev")
@PropertySource("file:${hidden.folder}/app-${spring.profiles.active}.properties")
public class configDev {
}
