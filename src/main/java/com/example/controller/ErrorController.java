package com.example.controller;

import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by nikp on 13.01.17.
 */
public class ErrorController {

    @RequestMapping(value="/error",method= RequestMethod.GET)
    public String renderErrorPage(final Model model, final HttpServletRequest request){

        //Get the Http error code.
        final int error_code=(int) request.getAttribute("javax.servlet.error.status_code");
        String message="";

        model.addAttribute("errorCode",error_code);
        return "error";
    }
}
