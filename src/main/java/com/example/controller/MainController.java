package com.example.controller;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @author npapapet
 */
@Controller
public class MainController {

    @RequestMapping("/")
    public String greeting(@RequestParam(value="name", required=false, defaultValue="World") String name, Model model) {
        model.addAttribute("name", name);
        return "index";
    }

    @RequestMapping(value = "/ldap/login", method = RequestMethod.GET)
    public String ldapLogin(@RequestParam(value = "error", required = false) String error, Model model) {
        boolean hey = SecurityContextHolder.getContext().getAuthentication().isAuthenticated();
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        /*if(SecurityContextHolder.getContext().getAuthentication().isAuthenticated()) {
            return "redirect:homePage";
        }*/
        model.addAttribute("error", error);
        return "login";
    }
}
