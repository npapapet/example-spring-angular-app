package com.example.controller;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.ldap.userdetails.LdapUserDetailsImpl;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author npapapet
 */
@Controller
public class HomePageController {

	protected final Logger log = LoggerFactory.getLogger(this.getClass());

	@RequestMapping("/homePage")
	public String homePage(Model model) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		model.addAttribute("username", 	auth.getName());

		if(auth.getPrincipal() instanceof LdapUserDetailsImpl){
			LdapUserDetailsImpl user = (LdapUserDetailsImpl) auth.getPrincipal();
			model.addAttribute("user", user.getUsername());
		}
		else {
			User user = (User) auth.getDetails();
			model.addAttribute("user", user.getUsername());
		}
		return "homePage";
	}

}
